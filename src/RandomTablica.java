import java.util.Arrays;
import java.util.Random;

public class RandomTablica {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(generateArray(100,2000)));

    }
    static public int [] generateArray(int size , int range) {
        Random random = new Random();
        int array[] = new int[size];
        for (int i = 0; i <size; i++){
            array[i] = random.nextInt(range);
        }
        return array;
    }
}
